-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 12, 2024 at 05:29 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bdd_cursos`
--

-- --------------------------------------------------------

--
-- Table structure for table `agencias`
--

CREATE TABLE `agencias` (
  `id` int(11) NOT NULL,
  `ciudad` varchar(150) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `horarioInicio` time DEFAULT NULL,
  `horarioFinal` time DEFAULT NULL,
  `latitud` double DEFAULT NULL,
  `longitud` double DEFAULT NULL,
  `provinciaId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `agencias`
--

INSERT INTO `agencias` (`id`, `ciudad`, `telefono`, `horarioInicio`, `horarioFinal`, `latitud`, `longitud`, `provinciaId`) VALUES
(1, 'Cuenca', '072836720 ', '09:00:00', '17:00:00', -2.8961763162615717, -79.0045171873725, 1),
(2, 'Latacunga', '032813762 ', '09:00:00', '17:00:00', -0.9309398695662108, -78.61664849871805, 5);

-- --------------------------------------------------------

--
-- Table structure for table `cajeros`
--

CREATE TABLE `cajeros` (
  `id` int(11) NOT NULL,
  `ciudad` varchar(150) DEFAULT NULL,
  `sistema` varchar(150) DEFAULT NULL,
  `funcion` varchar(500) DEFAULT NULL,
  `latitud` double DEFAULT NULL,
  `longitud` double DEFAULT NULL,
  `provinciaId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `cajeros`
--

INSERT INTO `cajeros` (`id`, `ciudad`, `sistema`, `funcion`, `latitud`, `longitud`, `provinciaId`) VALUES
(1, 'Guayaquil', 'XFS', 'Retiros', -2.2353404222765296, -79.8973925744932, 9),
(3, 'Guayaquil', 'XFS', 'Retiros', -2.133962300629552, -79.90292865389993, 9);

-- --------------------------------------------------------

--
-- Table structure for table `corresponsal`
--

CREATE TABLE `corresponsal` (
  `id` int(11) NOT NULL,
  `ciudad` varchar(150) DEFAULT NULL,
  `telefono` varchar(150) DEFAULT NULL,
  `tipoEntidad` varchar(150) DEFAULT NULL,
  `provinciaId` int(11) DEFAULT NULL,
  `latitud` double DEFAULT NULL,
  `longitud` double DEFAULT NULL,
  `logo` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `corresponsal`
--

INSERT INTO `corresponsal` (`id`, `ciudad`, `telefono`, `tipoEntidad`, `provinciaId`, `latitud`, `longitud`, `logo`) VALUES
(1, 'Cuenca', '0975966866', 'CAR', 1, -2.8905131084115894, -79.0243228113096, 'doctor_13424321783754.jpg'),
(3, 'xcvgbnm', '8523', 'gcvhbjnkml', 1, -0.9151919821794438, -78.64173251742532, 'doctor_8528312799116.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `provincias`
--

CREATE TABLE `provincias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) DEFAULT NULL,
  `codigo` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `provincias`
--

INSERT INTO `provincias` (`id`, `nombre`, `codigo`) VALUES
(1, 'Azuay', 1),
(2, 'Bolívar', 2),
(3, 'Cañar', 3),
(4, 'Carchi', 4),
(5, 'Cotopaxi', 5),
(6, 'Chimborazo', 6),
(7, 'El Oro', 7),
(8, 'Esmeraldas', 8),
(9, 'Guayas', 9),
(10, 'Imbabura', 10),
(11, 'Loja', 11),
(12, 'Los Ríos', 12),
(13, 'Manabí', 13),
(14, 'Morona Santiago', 14),
(15, 'Napo', 15),
(16, 'Pastaza', 16),
(17, 'Pichincha', 17),
(18, 'Tungurahua', 18),
(19, 'Zamora Chinchipe', 19),
(20, 'Galápagos', 20),
(21, 'Sucumbíos', 21),
(22, 'Orellana', 22),
(23, 'Santo Domingo de los Tsáchilas', 23),
(24, 'Santa Elena', 24);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agencias`
--
ALTER TABLE `agencias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provinciaId` (`provinciaId`);

--
-- Indexes for table `cajeros`
--
ALTER TABLE `cajeros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provinciaId` (`provinciaId`);

--
-- Indexes for table `corresponsal`
--
ALTER TABLE `corresponsal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provinciaId` (`provinciaId`);

--
-- Indexes for table `provincias`
--
ALTER TABLE `provincias`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agencias`
--
ALTER TABLE `agencias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cajeros`
--
ALTER TABLE `cajeros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `corresponsal`
--
ALTER TABLE `corresponsal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `provincias`
--
ALTER TABLE `provincias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `agencias`
--
ALTER TABLE `agencias`
  ADD CONSTRAINT `agencias_ibfk_1` FOREIGN KEY (`provinciaId`) REFERENCES `provincias` (`id`);

--
-- Constraints for table `cajeros`
--
ALTER TABLE `cajeros`
  ADD CONSTRAINT `cajeros_ibfk_1` FOREIGN KEY (`provinciaId`) REFERENCES `provincias` (`id`);

--
-- Constraints for table `corresponsal`
--
ALTER TABLE `corresponsal`
  ADD CONSTRAINT `corresponsal_ibfk_1` FOREIGN KEY (`provinciaId`) REFERENCES `provincias` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
