-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 13-02-2024 a las 01:47:21
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdd_cursos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agencias`
--

CREATE TABLE `agencias` (
  `id` int(11) NOT NULL,
  `ciudad` varchar(150) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `horarioInicio` time DEFAULT NULL,
  `horarioFinal` time DEFAULT NULL,
  `latitud` double DEFAULT NULL,
  `longitud` double DEFAULT NULL,
  `provinciaId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `agencias`
--

INSERT INTO `agencias` (`id`, `ciudad`, `telefono`, `horarioInicio`, `horarioFinal`, `latitud`, `longitud`, `provinciaId`) VALUES
(1, 'Cuenca', '072836720 ', '09:00:00', '17:00:00', -2.8961763162615717, -79.0045171873725, 1),
(2, 'Latacunga', '032813762 ', '09:00:00', '17:00:00', -0.9309398695662108, -78.61664849871805, 5),
(6, 'Quininde', '062995170', '09:00:00', '17:00:00', -0.9139046858594411, -78.61666995639017, 8),
(7, 'Otavalo', '062926147', '09:00:00', '17:00:00', 0.19799273376070708, -78.28716594332865, 10),
(8, 'Babahoyo', '052732542', '09:00:00', '17:00:00', -1.0406581462113509, -80.54485637301615, 12),
(9, 'Calderon', '022828876', '09:00:00', '17:00:00', 0.1574808445669457, -78.32081157321146, 17),
(10, 'Santo Domingo', '022761196', '09:00:00', '17:00:00', -0.25244550852672143, -79.2017777597349, 23),
(11, 'Riobamba', '32940733', '09:00:00', '17:00:00', -1.6721917952728615, -78.6799271737974, 6),
(12, 'Machala', '072968963 ', '09:00:00', '17:00:00', -3.244485380553839, -79.94610149020365, 7),
(13, 'Mall del Norte', '0990765765', '10:00:00', '18:00:00', -2.0839569048822253, -80.43636638278177, 9),
(14, 'Loja', '072586028', '09:00:00', '17:00:00', -3.9955418728699588, -79.19903117770365, 11),
(15, 'Portoviejo la Merced', '0990765765', '09:00:00', '17:00:00', -0.3835927516792597, -80.17200786227396, 13),
(16, 'Santa Elena', '042942663', '09:00:00', '17:00:00', -2.2239335830909055, -80.67669231051615, 24),
(17, 'Ambato Castillo', '032820135', '09:00:00', '17:00:00', -0.7742733441456761, -77.2077592050474, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cajeros`
--

CREATE TABLE `cajeros` (
  `id` int(11) NOT NULL,
  `ciudad` varchar(150) DEFAULT NULL,
  `sistema` varchar(150) DEFAULT NULL,
  `funcion` varchar(500) DEFAULT NULL,
  `latitud` double DEFAULT NULL,
  `longitud` double DEFAULT NULL,
  `provinciaId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `cajeros`
--

INSERT INTO `cajeros` (`id`, `ciudad`, `sistema`, `funcion`, `latitud`, `longitud`, `provinciaId`) VALUES
(1, 'Guayaquil', 'XFS', 'Retiros', -2.2353404222765296, -79.8973925744932, 9),
(3, 'Guayaquil', 'XFS', 'Retiros', -2.133962300629552, -79.90292865389993, 9),
(4, 'Ambato Castillo', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.9050652387019745, -78.60714274996927, 18),
(5, 'Ambato Ceballos', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.8819795911706371, -78.67005664462259, 18),
(6, 'Atahualpa', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.8939086241821544, -78.5828526651304, 17),
(8, 'Babahoyo', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.8850691285899926, -78.65237552279642, 12),
(9, 'Parque California', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.8795766158486773, -78.59538394564798, 9),
(10, 'Carapungo', 'XFS', 'Retiro de efectivo ', -0.9402512577070437, -78.66576511019876, 17),
(11, 'Carmen', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.9282365587151942, -78.60757190341165, 13),
(12, 'Cayambe', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.9447996687071043, -78.55976420993021, 17),
(13, 'Chone', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.9404228959657595, -78.64774066561868, 17),
(15, 'Cuenca', 'XFS', 'Retiro de efectivo', -0.9459153157799436, -78.631947818939, 1),
(16, 'Shopping Daule', 'XFS', 'Retiro de efectivo', -0.9496055304632605, -78.63169032687357, 9),
(17, 'El Lejido', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.8921922189292184, -78.59469730014017, 17),
(18, 'Esmeraldas', 'XFS', 'Retiro de efectivo ', -0.884296744476084, -78.63855678195169, 8),
(19, 'Ibarra', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.8916772971971342, -78.63649684542825, 10),
(20, 'Latacunga', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', 0, 0, 5),
(21, 'Libertad', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.9218000960014799, -78.6744340097349, 24),
(22, 'Loja', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.8961399498113297, -78.63443690890482, 11),
(23, 'Machala', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.9002593166301738, -78.63709766024759, 7),
(24, 'Mall del Sur', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.9114159116664375, -78.66542178744486, 9),
(25, 'Mall del Pacifico', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.8939944444237523, -78.61624080294779, 13),
(26, 'Otavalo', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.8853265899255546, -78.5578759347837, 10),
(27, 'Paseo Durán Shopping', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.9468593245614473, -78.66293269747904, 9),
(28, 'Portoviejo Olmedo', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.8807781037033275, -78.63915759677103, 13),
(29, 'Prensa', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.9217142764198599, -78.64259082431009, 17),
(30, 'Quevedo', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.9066958179597148, -78.63546687716654, 12),
(31, 'Quicentro Sur', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.910300253711025, -78.61624080294779, 17),
(32, 'Quininde', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.9161359991871723, -78.61967403048685, 8),
(33, 'Recreo 1', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.9214568176625828, -78.64756900424173, 17),
(34, 'Riobamba', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.9087554959722189, -78.61821490878275, 6),
(35, 'Santo Domingo', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.9111584521689181, -78.61941653842142, 23),
(36, 'Urdesa', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.9119308306062491, -78.61280757540872, 17),
(37, 'Mall del sol', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.9226582917039313, -78.64568072909525, 9),
(38, 'Santa Elena', 'XFS', 'Retiros, depósitos y pagos de tarjeta y crédito', -0.9095278749242782, -78.61915904635599, 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `corresponsal`
--

CREATE TABLE `corresponsal` (
  `id` int(11) NOT NULL,
  `ciudad` varchar(150) DEFAULT NULL,
  `telefono` varchar(150) DEFAULT NULL,
  `tipoEntidad` varchar(150) DEFAULT NULL,
  `provinciaId` int(11) DEFAULT NULL,
  `latitud` double DEFAULT NULL,
  `longitud` double DEFAULT NULL,
  `logo` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `corresponsal`
--

INSERT INTO `corresponsal` (`id`, `ciudad`, `telefono`, `tipoEntidad`, `provinciaId`, `latitud`, `longitud`, `logo`) VALUES
(1, 'Cuenca', '0975966866', 'CAR', 1, -2.8905131084115894, -79.0243228113096, 'doctor_13424321783754.jpg'),
(3, 'xcvgbnm', '8523', 'gcvhbjnkml', 1, -0.9151919821794438, -78.64173251742532, 'doctor_8528312799116.jpg'),
(13, 'Ambato', '022761196', 'Cuenta corriente', 3, -0.9178523931092931, -78.6469681894224, ''),
(14, 'Sucre', '052732542', 'Cuenta de ahorros', 13, 0, 0, ''),
(15, 'Quito', '052732542', 'Tarjeta de crédito', 17, -0.910300253711025, -78.61521083468607, ''),
(16, 'Cañar', '0998492894', 'Tarjeta de débito', 3, -0.9087554959722189, -78.6125500833433, ''),
(17, 'Chone', '0998581668', 'Préstamo personal', 13, -0.9107293529655177, -78.61821490878275, ''),
(18, 'Portoviejo ', '0998586834', 'Préstamo personal', 13, -0.9145054241995685, -78.62611133212259, ''),
(19, 'Montufar', '0987373773', 'Línea de crédito', 4, -0.9151919821794438, -78.62439471835306, ''),
(20, 'Atacamesq', '0987356787', '', 8, -0.9135614067628213, -78.62748462313822, ''),
(21, 'Baños de agua santa', '0998586834', 'Chequera', 18, -0.9132181276334163, -78.62259227389505, ''),
(22, 'Quito', '0998586834', 'Depósito a plazo fijo', 17, -0.9112442720034686, -78.62259227389505, ''),
(23, 'Guayaquil', '0998581668', 'Fondos de inversión', 9, -0.9328708046197949, -78.67494899386575, ''),
(24, 'Guaranda', '0990765765', 'Acciones', 2, -0.9134755869835488, -78.61958819979837, ''),
(25, 'Mejia', '0987478282', 'Bonos', 17, -0.912274109858538, -78.62087566012552, ''),
(27, 'Ambato', '0998581668', 'Seguro de vida', 18, -0.9126173890780671, -78.6169274484556, ''),
(28, 'Quininde', '052732542', 'Seguro de hogar', 8, 0, 0, ''),
(29, 'Shushufind', '0998586834', 'Seguro de automóvil', 21, -0.9142479649232502, -78.61126262301615, ''),
(30, 'Tulcan', '0998586834', 'Seguro de salud', 4, -0.9105577132698528, -78.61589748019388, ''),
(31, 'Cayambe', '0998492894', 'Fideicomiso', 17, -0.9130464880564242, -78.62001735324075, ''),
(32, 'El Carmen', '0998586834', 'Administración de patrimonio', 13, -0.9136472265400581, -78.62456637973001, ''),
(33, 'Orellana', '022761196', 'Plan de jubilación', 22, -0.9135614067628213, -78.62173396701029, ''),
(34, 'Las Joyas', '0998586834', 'Pago de servicios públicos', 22, -0.9127890286755548, -78.61864406222513, ''),
(35, 'La Mana', '0998492894', 'Hipoteca inversa', 5, -0.9136472265400581, -78.61890155429056, ''),
(36, 'La Troncal', '052732542', 'Cuenta corriente', 3, -0.9143337846840756, -78.62164813632181, ''),
(37, 'Latacunga', '0990765765', 'Cuenta de ahorros', 5, -0.9115017314948559, -78.61117679232767, ''),
(38, 'Morona', '0998492894', 'Tarjeta de crédito', 14, -0.9139905056284722, -78.6195023691099, ''),
(39, 'Manta', '08388483928', 'Tarjeta de débito', 13, -0.910643533118703, -78.61194926852396, ''),
(40, 'Roca Fuerte', '0990765765', 'Préstamo personal', 13, -0.9114159116664375, -78.62044650668314, ''),
(41, 'Salcedo', '0990765765', 'Préstamo hipotecario', 5, -0.9090129556412719, -78.6132367288511, ''),
(42, 'Saquisili', '0998492894', 'Línea de crédito', 5, -0.9084122163849667, -78.60534030551126, ''),
(43, 'Guayaquil', '052732542', 'Chequera', 9, -0.9310685986027107, -78.68499118441751, ''),
(44, 'Tena', '0998492894', 'Depósito a plazo fijo', 15, -0.9066958179597148, -78.60911685580423, ''),
(45, 'Riobamba', '0998581668', 'Fondos de inversión', 6, -0.9107293529655177, -78.61933070773294, ''),
(46, 'Duran', '0998492894', 'Acciones', 9, -0.9127890286755548, -78.61538249606302, ''),
(47, 'Latacunga', '0998586834', 'Bonos', 5, -0.9096136947976314, -78.61152011508157, ''),
(48, 'Ambato', '0998586834', 'Seguro de vida', 18, -0.9221433743072867, -78.6473115121763, ''),
(49, 'Manta', '0998586834', 'Seguro de hogar', 13, -0.9246321410336074, -78.65821200961282, ''),
(50, 'Quito', '0990765765', 'Cuenta corriente', 17, -0.9270350865621664, -78.65752536410501, ''),
(51, 'Tena', '0998492894', 'Cuenta de ahorros', 15, -0.9133897672022281, -78.62465221041849, ''),
(52, 'Otavalo', '0998586834', 'Tarjeta de crédito', 10, -0.9114159116664375, -78.62121898287943, ''),
(53, 'Cuenca', '0990765765', 'Tarjeta de débito', 1, -0.9218000960014799, -78.65048724764993, ''),
(54, 'Riobamba', '0998586834', 'Préstamo personal', 6, -0.9081547566731162, -78.61049014681986, ''),
(55, 'Santo Domingo', '0998581668', 'Préstamo hipotecario', 23, -0.9269492671070687, -78.6637910043638, ''),
(56, 'Cayambe', '0998586834', 'Línea de crédito', 17, -0.9101286139949281, -78.61555415743997, ''),
(57, 'Macara', '0990765765', 'Chequera', 11, -0.909098775526864, -78.61340839022806, ''),
(58, 'Manta', '0987356787', 'Depósito a plazo fijo', 13, -0.9184531307875089, -78.6674817239683, ''),
(59, 'Guaranda', '0998492894', 'Fondos de inversión', 2, -0.9219717351585244, -78.655722919647, ''),
(60, 'Guayaquil', '0998581668', 'Acciones', 9, -0.9193113272954803, -78.65529376620462, ''),
(61, 'Biblian', '062995170', 'Cuenta corriente', 3, -0.9119308306062491, -78.61975986117532, ''),
(62, 'Eloy Alfaro', '0990765765', 'Cuenta de ahorros', 8, -0.9130464880564242, -78.62181979769876, ''),
(63, 'Eloy Alfaro', '0998492894', 'Tarjeta de débito', 17, -0.9127032088778351, -78.6213048135679, ''),
(64, 'Rumiñahui', '9005454545', 'Préstamo hipotecario', 17, -0.9013749776769455, -78.6085160409849, ''),
(65, 'Riobamba', '0990765765', 'Línea de crédito', 6, -0.9120166504223906, -78.61332255953958, ''),
(66, 'Archidona', '0998492894', 'Chequera', 15, -0.9204269824474501, -78.65074473971536, ''),
(67, 'Ambato', '0987373773', 'Depósito a plazo fijo', 18, -0.9135614067628213, -78.62345058077982, ''),
(68, 'Ibarra', '0998586834', 'Cuenta corriente', 10, -0.9111584521689181, -78.62224895114115, ''),
(69, 'Cotacachi', '0998492894', 'Tarjeta de crédito', 10, -0.910643533118703, -78.61649829501322, ''),
(70, 'Latacunga', '0998586834', 'Tarjeta de débito', 5, 0, 0, ''),
(71, 'Cayambe', '0998492894', 'Préstamo personal', 17, -0.9138188660883617, -78.62430888766458, ''),
(72, 'Rumiñahui', '052732542', 'Préstamo hipotecario', 17, -0.9137330463152341, -78.61761409396341, ''),
(73, 'Cayambe', '0998586834', 'Línea de crédito', 17, -0.9115017314948559, -78.61890155429056, ''),
(74, 'Chone', '0998586834', 'Chequera', 13, -0.9130464880564242, -78.62036067599466, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincias`
--

CREATE TABLE `provincias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) DEFAULT NULL,
  `codigo` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `provincias`
--

INSERT INTO `provincias` (`id`, `nombre`, `codigo`) VALUES
(1, 'Azuay', 1),
(2, 'Bolívar', 2),
(3, 'Cañar', 3),
(4, 'Carchi', 4),
(5, 'Cotopaxi', 5),
(6, 'Chimborazo', 6),
(7, 'El Oro', 7),
(8, 'Esmeraldas', 8),
(9, 'Guayas', 9),
(10, 'Imbabura', 10),
(11, 'Loja', 11),
(12, 'Los Ríos', 12),
(13, 'Manabí', 13),
(14, 'Morona Santiago', 14),
(15, 'Napo', 15),
(16, 'Pastaza', 16),
(17, 'Pichincha', 17),
(18, 'Tungurahua', 18),
(19, 'Zamora Chinchipe', 19),
(20, 'Galápagos', 20),
(21, 'Sucumbíos', 21),
(22, 'Orellana', 22),
(23, 'Santo Domingo de los Tsáchilas', 23),
(24, 'Santa Elena', 24);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `agencias`
--
ALTER TABLE `agencias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provinciaId` (`provinciaId`);

--
-- Indices de la tabla `cajeros`
--
ALTER TABLE `cajeros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provinciaId` (`provinciaId`);

--
-- Indices de la tabla `corresponsal`
--
ALTER TABLE `corresponsal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provinciaId` (`provinciaId`);

--
-- Indices de la tabla `provincias`
--
ALTER TABLE `provincias`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `agencias`
--
ALTER TABLE `agencias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `cajeros`
--
ALTER TABLE `cajeros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `corresponsal`
--
ALTER TABLE `corresponsal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT de la tabla `provincias`
--
ALTER TABLE `provincias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `agencias`
--
ALTER TABLE `agencias`
  ADD CONSTRAINT `agencias_ibfk_1` FOREIGN KEY (`provinciaId`) REFERENCES `provincias` (`id`);

--
-- Filtros para la tabla `cajeros`
--
ALTER TABLE `cajeros`
  ADD CONSTRAINT `cajeros_ibfk_1` FOREIGN KEY (`provinciaId`) REFERENCES `provincias` (`id`);

--
-- Filtros para la tabla `corresponsal`
--
ALTER TABLE `corresponsal`
  ADD CONSTRAINT `corresponsal_ibfk_1` FOREIGN KEY (`provinciaId`) REFERENCES `provincias` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
