<div id="header-carousel" class="carousel slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item position-relative active" style="min-height: 100vh;">
            <img class="position-absolute w-100 h-100" src="<?php echo base_url('/assets/images/solidario.jpg');?>" style="object-fit: cover;">
            <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
            <div class="p-3" style="max-width: 700px; margin-left: 220px; background-color: black; opacity: 0.7; border-radius: 10px;">
                    <h4 class="text-white text-uppercase mb-3 animate__animated animate__fadeInDown" style="letter-spacing: 3px;">"Banco Solidario"</h4>
                    <h5 class="display-6 text-capitalize text-white mb-3">Brinda a las personas acceso <br><br>xoportuno a productos financieros<br><br> como: créditos personales, créditos de <br><br>microempresa,créditos prendarios y<br><br> mucho mas .....</h5>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>



<div class="container mt-4">
    <div class="row">
        <div class="col-md-3 mb-5">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="..." alt="." width="200" height="200">
                <div class="card-body">
                    <p class="card-text"></p>
                </div>
            </div>
        </div>
        <div class="col-md-3 mb-5">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="<?php echo base_url('/assets/images/solidario1.jpg');?>" alt="Card image cap" width="300" height="200">
                <div class="card-body">
                   <center> <h1>Misión</h1></center>
                    <p class="card-text">Contribuir al progreso y a la mejora de la calidad de vida de los microempresarios y trabajadores de los grandes segmentos de la población ecuatoriana, con productos y servicios financieros adecuados a sus necesidades, a través de un equipo humano que hace de esta misión parte de su vida.</p>
                </div>
            </div>
        </div>

        <div class="col-md-3 mb-5">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="<?php echo base_url('/assets/images/solidario2.png');?>" alt="." width="300" height="200">
                <div class="card-body">
                    <center><h1>Principios</h1></center>

                </div>
            </div>
        </div>
        <div class="col-md-3 mb-5">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="<?php echo base_url('/assets/images/solidario3.png');?>" alt="Card image cap" width="300" height="200">
                <div class="card-body">
                    <center><h1>Visión</h1></center>
                    <p class="card-text">Liderar la inclusión financiera y social de grandes segmentos de la población, construyendo una red de relaciones de mutuo beneficio.</p>
                </div>
            </div>
        </div>
    </div>
</div>
