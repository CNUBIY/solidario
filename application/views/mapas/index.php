
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Mapa</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                            <li class="breadcrumb-item active">Mapa</li>
                        </ol>
                    </div>
                    <div class="col-md-6 col-4 align-self-center">
                        <button type="button" name="button" onclick="mostrarAgencias()" class="btn pull-right hidden-sm-down btn-success">Agencias</button>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" name="button" onclick="mostrarCajeros()" class="btn pull-right hidden-sm-down btn-success">Cajeros</button>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" name="button" onclick="mostrarCorresponsales()" class="btn pull-right hidden-sm-down btn-success">Correponsales</button>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- column -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Mapas</h4>
                                <h6 class="card-subtitle">UTC -<code>Banco Solidario</code></h6>
                                <div class="table-responsive" id="reporteMapa" style="width:100%; height:400px;">

                                </div>
                                <br>
                                <button type="button" onclick="mostrarTodosLosMarcadores()" class="btn pull-right hidden-sm-down btn-info">Mostrar Todos</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
<script type="text/javascript">
        var marcadoresAgencias = [];
        var marcadoresCajeros = [];
        var marcadoresCorresponsales = [];

        function initMap() {
            var coordenadaCentral = new google.maps.LatLng(-0.152948869329262, -78.4868431364856);
            var miMapa = new google.maps.Map(
                document.getElementById('reporteMapa'), {
                    center: coordenadaCentral,
                    zoom: 7,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
            );

            <?php foreach ($listadoAgencias as $agencia): ?>
                var coordenadaAgencia = new google.maps.LatLng(<?php echo $agencia->latitud; ?>, <?php echo $agencia->longitud; ?>);
                var svgIcon = `
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <path fill="#fff" d="M11,21H6V11h5V21z M17,21h-5V11h5V21z M18,9h-4V7c0-0.55-0.45-1-1-1h-4c-0.55,0-1,0.45-1,1v2H6C4.9,9,4,9.9,4,11v10c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V11C20,9.9,19.1,9,18,9z"/>
                </svg>
                `;


                var marcadorAgencia = new google.maps.Marker({
                    position: coordenadaAgencia,
                    map: miMapa,
                    title: 'Agencia #<?php echo $agencia->id; ?>',
                    icon: {
                      url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(svgIcon),
                      scaledSize: new google.maps.Size(24, 24), // Tamaño del icono (ajustar según sea necesario)
                    }
                });
                marcadoresAgencias.push(marcadorAgencia);
            <?php endforeach; ?>

            <?php foreach ($listadoCajeros as $cajero): ?>
                var coordenadaCajero = new google.maps.LatLng(<?php echo $cajero->latitud; ?>, <?php echo $cajero->longitud; ?>);
                var svgIconCajero = `
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <path fill="#ffffff" d="M12 2c-5.52 0-10 4.48-10 10s4.48 10 10 10 10-4.48 10-10-4.48-10-10-10zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zm-1-14v3h-1c-1.1 0-2 .9-2 2s.9 2 2 2h1v3c-1.66 0-3 1.34-3 3s1.34 3 3 3 3-1.34 3-3v-1h1c1.1 0 2-.9 2-2s-.9-2-2-2h-1v-3c1.66 0 3-1.34 3-3s-1.34-3-3-3zm1 10h-2v-2h2v2z"/>
                </svg>
                `;
                var marcadorCajero = new google.maps.Marker({
                    position: coordenadaCajero,
                    map: miMapa,
                    title: 'Cajero #<?php echo $cajero->id; ?>',
                    icon: {
                      url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(svgIconCajero),
                      scaledSize: new google.maps.Size(24, 24) // Tamaño del icono (ajustar según sea necesario)
                    }
                });
                marcadoresCajeros.push(marcadorCajero);
            <?php endforeach; ?>

            <?php foreach ($listadoCorresponsal as $corresponsal): ?>
                var coordenadaCorresponsal = new google.maps.LatLng(<?php echo $corresponsal->latitud; ?>, <?php echo $corresponsal->longitud; ?>);
                var svgIconCorresponsal = `
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <path fill="#ffffff" d="M12,3C7.03,3,3,7.03,3,12c0,4.97,4.03,9,9,9c4.97,0,9-4.03,9-9C21,7.03,16.97,3,12,3z M12,20c-4.41,0-8-3.59-8-8c0-4.41,3.59-8,8-8c4.41,0,8,3.59,8,8C20,16.41,16.41,20,12,20z M12,7c-1.65,0-3,1.35-3,3s1.35,3,3,3s3-1.35,3-3S13.65,7,12,7z M13,13H11v-2h2V13z"/>
                </svg>
                  `;
                var marcadorCorresponsal = new google.maps.Marker({
                    position: coordenadaCorresponsal,
                    map: miMapa,
                    title: 'Corresponsal #<?php echo $corresponsal->id; ?>',
                    icon: {
                    url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(svgIconCorresponsal),
                    scaledSize: new google.maps.Size(24, 24) // Tamaño del icono (ajustar según sea necesario)
                }
                });
                marcadoresCorresponsales.push(marcadorCorresponsal);
            <?php endforeach; ?>
        }

        function mostrarAgencias() {
            // Ocultar todos los marcadores
            ocultarTodosLosMarcadores();
            // Mostrar solo los marcadores de agencias
            for (var i = 0; i < marcadoresAgencias.length; i++) {
                marcadoresAgencias[i].setVisible(true);
            }
        }

        function mostrarCajeros() {
            // Ocultar todos los marcadores
            ocultarTodosLosMarcadores();
            // Mostrar solo los marcadores de cajeros
            for (var i = 0; i < marcadoresCajeros.length; i++) {
                marcadoresCajeros[i].setVisible(true);
            }
        }

        function mostrarCorresponsales() {
            // Ocultar todos los marcadores
            ocultarTodosLosMarcadores();
            // Mostrar solo los marcadores de corresponsales
            for (var i = 0; i < marcadoresCorresponsales.length; i++) {
                marcadoresCorresponsales[i].setVisible(true);
            }
        }

        function ocultarTodosLosMarcadores() {
            // Ocultar todos los marcadores de todas las categorías
            for (var i = 0; i < marcadoresAgencias.length; i++) {
                marcadoresAgencias[i].setVisible(false);
            }
            for (var i = 0; i < marcadoresCajeros.length; i++) {
                marcadoresCajeros[i].setVisible(false);
            }
            for (var i = 0; i < marcadoresCorresponsales.length; i++) {
                marcadoresCorresponsales[i].setVisible(false);
            }
        }
        function mostrarTodosLosMarcadores() {
          // Mostrar todos los marcadores de todas las categorías
          for (var i = 0; i < marcadoresAgencias.length; i++) {
            marcadoresAgencias[i].setVisible(true);
          }
          for (var i = 0; i < marcadoresCajeros.length; i++) {
            marcadoresCajeros[i].setVisible(true);
          }
          for (var i = 0; i < marcadoresCorresponsales.length; i++) {
            marcadoresCorresponsales[i].setVisible(true);
          }
        }
    </script>
