
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Edit</h3>
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                          <li class="breadcrumb-item"><a href="<?php echo site_url('cajeros/index'); ?>">Positions</a></li>
                          <li class="breadcrumb-item active">Edit Position</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> <img src="<?php echo base_url('assets/images/posicion.webp'); ?>" class="img-circle" width="80%" />
                                    <h4 class="card-title m-t-10">Players Positions</h4>
                                    <h6 class="card-subtitle">Edit the Position</h6>
                                </center>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                                <form action="<?php echo site_url('cajeros/actualizarPosicion'); ?>" id="frm_nuevo_cajero"  enctype="multipart/form-data" method="post" class="form-horizontal form-material">
                                  <input type="hidden" name="id_pos" id="id_pos" value="<?php echo $posicionEditar->id_pos; ?>">
                                  <div class="form-group">
                                        <label class="col-md-12"><b>Position's Name:</b></label>
                                        <div class="col-md-12">
                                            <input required type="text" name="nombre_pos" id="nombre_pos" value="<?php echo $posicionEditar->nombre_pos; ?>" placeholder="Ingrese el nombre de la posición" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <script>
                                    document.getElementById('ciudad').onkeypress = function(e) {
                                      var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                      var charTyped = String.fromCharCode(charCode);
                                      var regex = /^[A-Za-záéíóúÁÉÍÓÚ\s]+$/;
                                      if (!regex.test(charTyped) && charCode !== 8 && charCode !== 0) {
                                        e.preventDefault();
                                      }
                                    };
                                    </script>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12"><b>Description of the position:</b></label>
                                        <div class="col-md-12">
                                          <br>
                                          <textarea class="form-control" name="descripcion_pos" id="descripcion_pos" value="" placeholder="Describe the function of the position"><?php echo $posicionEditar->descripcion_pos; ?></textarea>  
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <center>
                                              <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-pen"></i> &nbsp Actualizar</button> &nbsp &nbsp
                                              <a href="<?php echo site_url('cajeros/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark"></i> &nbsp Cancelar</a>
                                          </center>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>

            <script type="text/javascript">
        $("#frm_nuevo_cajero").validate({
          rules:{
            "id_provincia":{
              required:true
            },
            "ciudad":{
              required:true,
            },
            "sistema":{
              required:true
            },
            "funcion":{
              required:true
            },
            "latitud":{
              required :true
            },
            "longitud":{
              required:true
            }
          },
          messages:{
            "id_provincia":{
              required:"Debe seleccionar la provincia"
            },
            "ciudad":{
              required:"Ingrese la ciudad",
            },
            "sistema":{
              required:"Ingrese el sistema del cajero"
            },
            "funcion":{
              required:"Ingrese la función del cajero"
            },
            "latitud":{
              required :"Ingrese la latitud"
            },
            "longitud":{
              required:"Ingrese la longitud"
            }
          }
        });
      </script>
