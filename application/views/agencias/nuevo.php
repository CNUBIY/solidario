
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Add Team</h3>
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                          <li class="breadcrumb-item"><a href="<?php echo site_url('agencias/index'); ?>">Teams</a></li>
                          <li class="breadcrumb-item active">Add Team</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> <img src="<?php echo base_url('assets/images/team.png'); ?>" class="img-circle" width="150" />
                                    <h4 class="card-title m-t-10">Soccer Teams</h4>
                                    <h6 class="card-subtitle">Add new soccer team</h6>
                                </center>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                                <form action="<?php echo site_url('agencias/guardarEquipo'); ?>" id="frm_nueva_agencia"  enctype="multipart/form-data" method="post" class="form-horizontal form-material">
                                  <div class="form-group">
                                      <label class="col-md-12"><b>Name Team:</b></label>
                                      <div class="col-md-12">
                                        <input type="text" name="nombre_equi" id="nombre_equi" placeholder="Ingrese el nombre del equipo" class="form-control form-control-line">
                                      </div>                                  
                                  </div>

                                    <div class="form-group">
                                        <label class="col-md-12"><b>Acronyms Team:</b></label>
                                        <div class="col-md-12">
                                            <input type="text" name="siglas_equi" id="siglas_equi" placeholder="Example: UFC" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12"><b>Fundation Team:</b></label>
                                        <div class="col-md-6">
                                            <input type="date" name="fundacion_equi" id="fundacion_equi" placeholder="09********" class="form-control form-control-line" name="example-email" id="example-email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12"><b>Region Team:</b></label>
                                        <div class="col-md-12">
                                            <input type="text" name="region_equi" id="region_equi" placeholder="Ingrese la región del equipo" class="form-control form-control-line" name="example-email" id="example-email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12"><b>Championships Team:</b></label>
                                        <div class="col-md-3">
                                            <input type="number" name="numero_titulos_equi" id="numero_titulos_equi" placeholder="" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <center>
                                              <button type="submit" name="button" class="btn btn-outline-info">
                                                <i class="fa fa-save"></i>
                                                Guardar</button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="<?php echo site_url('agencias/index'); ?>" class="btn btn-outline-danger">
                                                  <i class="fa fa-times"></i>
                                                  Cancelar</a>
                                          </center>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>

            <script type="text/javascript">
              function initMap(){
                var coordenadaCentral= new google.maps.LatLng(-0.9170800159461989, -78.63323527926615);
                var miMapa=new google.maps.Map(
                  document.getElementById('mapa'),
                  {
                    center: coordenadaCentral,
                    zoom: 14,
                    mapTypeId:google.maps.MapTypeId.ROADMAP,
                  }
                );
                var marcador = new google.maps.Marker(
                  {
                    position: coordenadaCentral,
                    map:miMapa,
                    title:'Selecciona la ubicación',
                    draggable:true
                  }
                );

                google.maps.event.addListener(
                  marcador,
                  'dragend',
                  function(event){
                    var latitud=this.getPosition().lat();
                    var longitud=this.getPosition().lng();
                    document.getElementById('latitud').value=latitud
                    document.getElementById('longitud').value=longitud
                  }
                );
              }


            </script>
            <script type="text/javascript">
        $("#frm_nueva_agencia").validate({
          rules:{
            "id_provincia":{
              required:true
            },
            "ciudad":{
              required:true,
            },
            "telefono":{
              required:true,
              minlength:9,
              maxlength:10
            },
            "horarioInicio":{
              required:true
            },
            "horarioFinal":{
              required:true
            },
            "latitud":{
              required :true
            },
            "longitud":{
              required:true
            }
          },
          messages:{
            "id_provincia":{
              required:"Debe seleccionar la provincia"
            },
            "ciudad":{
              required:"Ingrese la ciudad",
            },
            "telefono":{
              required:"Ingrese el teléfono",
              minlength:"El teléfono debe tener un mínimo de 9 números",
              maxlength:"El teléfono debe tener un máximo de 10 números"
            },
            "horarioInicio":{
              required:"Ingrese el horario de inicio"
            },
            "horarioFinal":{
              required:"Ingrese el horario de cierre"
            },
            "latitud":{
              required :"Ingrese la latitud"
            },
            "longitud":{
              required:"Ingrese la longitud"
            }
          }
        });
      </script>
