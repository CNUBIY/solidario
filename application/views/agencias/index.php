
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Soccer Teams</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                            <li class="breadcrumb-item active">Teams</li>
                        </ol>
                    </div>
                    <div class="col-md-6 col-4 align-self-center">
                        <a href="<?php echo site_url('agencias/nuevo') ?>" class="btn pull-right hidden-sm-down btn-success">Add Team</a>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- column -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Soccer Teams</h4>
                                <h6 class="card-subtitle">UTC -<code>SOCCER</code></h6>
                                <div class="table-responsive">
                                    <?php if ($listadoEquipos): ?>
                                    <table class="table" id="tbl_3">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Team Name</th>
                                                <th>Acronyms Team</th>
                                                <th>Fundation</th>
                                                <th>Region</th>
                                                <th>Championships</th>    
                                                <th>Acciones</th>                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php foreach ($listadoEquipos as $agencias): ?>
                                            <tr>
                                              <td><?php echo $agencias->id_equi; ?></td>
                                              <td><?php echo $agencias->nombre_equi; ?></td>
                                              <td><?php echo $agencias->siglas_equi; ?></td>
                                              <td><?php echo $agencias->fundacion_equi; ?></td>
                                              <td><?php echo $agencias->region_equi; ?></td>
                                              <td><?php echo $agencias->numero_titulos_equi; ?></td>                   
                                              <td>
                                                <a href="<?php echo site_url('agencias/editar/').$agencias->id_equi; ?>" class="btn btn-warning" title="Editar"><i class="fa fa-pen"></i></a>
                                                <a class="btn btn-danger delete-btn" href="<?php echo site_url('agencias/borrar/').$agencias->id_equi; ?>" title="Eliminar"><i class="fa fa-trash"></i></a>
                                              </td>
                                            </tr>
                                          <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                  <?php else: ?>
                                    <div class="alert alert-danger">
                                      No se encontró agencias registrados
                                    </div>
                                  <?php endif; ?>
                                </div><br>
                                <button id="toggleChartButton" onclick="toggleChart()" class="btn btn-outline-info">Mostrar/ocultar gráfico</button>
                                <div id="chartContainer" style="width: 50%; margin: auto; border: 1px solid black; padding: 10px; display: none;">
                                  <canvas id="myChart"></canvas>
                                  <canvas id="myPieChart"></canvas>
                                </div><br>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <script type="text/javascript">
                    $(document).ready(function() {
                        $('#tbl_3').DataTable( {
                            dom: 'Bfrtip',
                            buttons: [
                                {
                                    extend: 'pdfHtml5',
                                    text: '<i class="fa-solid fa-file-pdf"></i> Exportar a PDF',
                                    className: 'btn btn-outline-info',
                                    messageTop: 'REPORTE DE AGENCIAS ',
                                    title:'INFORMACIÓN Banco Solidario'
                                },
                                {
                                    extend: 'print',
                                    text: '<i class="fa-solid fa-print"></i> Imprimir',
                                    className: 'btn btn-outline-info',
                                    messageTop: 'REPORTE DE AGENCIAS ',
                                    title:'INFORMACIÓN Banco Solidario'
                                },
                                {
                                    extend: 'csv',
                                    text: '<i class="fa-solid fa-file-csv"></i> Exportar a CSV',
                                    className: 'btn btn-outline-info',
                                    messageTop: 'REPORTE DE AGENCIAS ',
                                    title:'INFORMACIÓN Banco Solidario'
                                }
                            ],
                            language: {
                                url: "https://cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json"
                            }
                        } );
                    } );
                </script>



                <script>
                function toggleChart() {
                    var chartContainer = document.getElementById('chartContainer');
                    if (chartContainer.style.display === 'none') {
                        chartContainer.style.display = 'block';
                    } else {
                        chartContainer.style.display = 'none';
                    }
                }

                document.addEventListener('DOMContentLoaded', function () {
      const ctx = document.getElementById('myChart');
      const pieCtx = document.getElementById('myPieChart');
      const provincias = [];
      const cantidadAgencias = [];

      <?php
          // Inicializamos un array para almacenar la cantidad de agencias por provincia
          $agenciasPorProvincia = array();
          foreach ($listadoAgencias as $agencia) {
              // Verificamos si ya existe la provincia en el array
              if (array_key_exists($agencia->nombre_provincia, $agenciasPorProvincia)) {
                  // Si existe, incrementamos la cantidad de agencias
                  $agenciasPorProvincia[$agencia->nombre_provincia]++;
              } else {
                  // Si no existe, inicializamos la cantidad en 1
                  $agenciasPorProvincia[$agencia->nombre_provincia] = 1;
              }
          }
      ?>

      <?php foreach ($agenciasPorProvincia as $provincia => $cantidad): ?>
          provincias.push("<?php echo $provincia; ?>");
          cantidadAgencias.push(<?php echo $cantidad; ?>);
      <?php endforeach; ?>

      // Genera el gráfico de barras
      new Chart(ctx, {
          type: 'bar',
          data: {
              labels: provincias,
              datasets: [{
                  label: 'Cantidad de Agencias',
                  data: cantidadAgencias,
                  backgroundColor: 'rgba(75, 192, 192, 0.5)',
                  borderColor: 'rgba(75, 192, 192, 1)',
                  borderWidth: 1,
              }]
          },
          options: {
              scales: {
                  x: {
                      type: 'category',
                      position: 'bottom',
                  },
                  y: {
                      beginAtZero: true,
                      stepSize: 1,
                  }
              }
          }
      });

      // Genera el gráfico de torta
      new Chart(pieCtx, {
          type: 'pie',
          data: {
              labels: provincias,
              datasets: [{
                  label: 'Cantidad de Agencias',
                  data: cantidadAgencias,
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.5)',
                      'rgba(54, 162, 235, 0.5)',
                      'rgba(255, 206, 86, 0.5)',
                      'rgba(75, 192, 192, 0.5)',
                      'rgba(153, 102, 255, 0.5)',
                      'rgba(255, 159, 64, 0.5)'
                  ],
                  borderColor: [
                      'rgba(255, 99, 132, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      'rgba(75, 192, 192, 1)',
                      'rgba(153, 102, 255, 1)',
                      'rgba(255, 159, 64, 1)'
                  ],
                  borderWidth: 1,
              }]
          }
      });
  });
                </script>

                <script>
                $(document).ready(function() {
    $('.delete-btn').click(function(event) {
        // Evitar el comportamiento predeterminado del enlace
        event.preventDefault();

        var id = $(this).data('id_equi');
        Swal.fire({
            title: '¿Estás seguro de que quieres eliminar este registro?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Sí',
            denyButtonText: 'No',
            customClass: {
                actions: 'my-actions',
                cancelButton: 'order-1 right-gap',
                confirmButton: 'order-2',
                denyButton: 'order-3',
            },
        }).then((result) => {
            if (result.isConfirmed) {
                // Realizar la acción de eliminación
                // Por ejemplo, redireccionar a una URL que maneje la eliminación
                window.location.href = "<?php echo site_url('agencias/borrar/').$agencias->id_equi; ?>"
                // No redirigir aquí, dejar que el servidor maneje la redirección después de eliminar
            } else if (result.isDenied) {
                // No hacer nada si el usuario cancela la eliminación
            }
        });
    });
});
  </script>
