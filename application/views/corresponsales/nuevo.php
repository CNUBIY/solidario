
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Add</h3>
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                          <li class="breadcrumb-item"><a href="<?php echo site_url('corresponsales/index'); ?>">Players</a></li>
                          <li class="breadcrumb-item active">New Player</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> <img src="<?php echo base_url('assets/images/player.webp'); ?>" class="img-circle" width="80%" />
                                    <h4 class="card-title m-t-10">Soccer Players</h4>
                                    <h6 class="card-subtitle">Add new Players</h6>
                                </center>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                                <form id="frm_nuevo_corresponsal" action="<?php echo site_url('corresponsales/guardarJugador'); ?>"  enctype="multipart/form-data" method="post" class="form-horizontal form-material">
                                    <div class="form-group">
                                        <label class="col-md-12"><b>Player's Name: </b></label>
                                        <div class="col-md-12">
                                            <input required type="text" name="nombre_jug" id="nombre_jug" placeholder="Ingrese el nombre del jugador" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12"><b>Player's Lastname: </b></label>
                                        <div class="col-md-12">
                                            <input required type="text" name="apellido_jug" id="apellido_jug" placeholder="Ingrese el nombre del jugador" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <script>
                                    document.getElementById('ciudad').onkeypress = function(e) {
                                      var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                      var charTyped = String.fromCharCode(charCode);
                                      var regex = /^[A-Za-záéíóúÁÉÍÓÚ\s]+$/;
                                      if (!regex.test(charTyped) && charCode !== 8 && charCode !== 0) {
                                        e.preventDefault();
                                      }
                                    };
                                    </script>

                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12"><b>Player's Height:</b></label>
                                        <div class="col-md-12">
                                            <input required type="text" name="estatura_jug" id="estatura_jug" placeholder="Example: 1.80" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12"><b>Player's Salary:</b></label>
                                        <div class="col-md-12">
                                            <input required type="text" name="salario_jug" id="salario_jug" placeholder="Ingrese el salario del jugador" class="form-control form-control-line" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12"><b>Player's status:</b></label>
                                        <div class="col-md-12">
                                            <select required name="estado_jug" id="estado_jug" class="form-control form-control-line">
                                              <option value="">Select the status:</option>
                                              <option value="Active">Active</option>
                                              <option value="Inactive">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-md-12"><b>Player's Position:</b></label>
                                      <div class="col-md-12">
                                        <select required name="id_pos"  id="id_pos" class="form-control form-control-line">
                                          <option value="">Select the position:</option>
                                          <?php foreach ($listadoPosicion as $provincias): ?>
                                            <option value="<?php echo $provincias->id_pos; ?>"><?php echo $provincias->nombre_pos; ?></option>
                                          <?php endforeach; ?>
                                        </select>
                                      </div>
                                  </div>
                                  <script type="text/javascript">
                                  $("#id_pos").select2({
                                    maximumSelectionLength: 2
                                  });
                                  </script>
                                  <div class="form-group">
                                      <label class="col-md-12"><b>Player's Team:</b></label>
                                      <div class="col-md-12">
                                        <select required name="id_equi"  id="id_equi" class="form-control form-control-line">
                                          <option value="">Select the Team:</option>
                                          <?php foreach ($listadoEquipo as $provincias): ?>
                                            <option value="<?php echo $provincias->id_equi; ?>"><?php echo $provincias->nombre_equi; ?> - <?php echo $provincias->siglas_equi; ?></option>
                                          <?php endforeach; ?>
                                        </select>
                                      </div>
                                  </div>
                                  <script type="text/javascript">
                                  $("#id_equi").select2({
                                    maximumSelectionLength: 2
                                  });
                                  </script>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <center>
                                              <button type="submit" name="button" class="btn btn-outline-info">
                                                <i class="fa fa-save"></i>
                                                Guardar</button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="<?php echo site_url('corresponsales/index'); ?>" class="btn btn-outline-danger">
                                                  <i class="fa fa-times"></i>
                                                  Cancelar</a>
                                          </center>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>

            <script type="text/javascript">
              function initMap(){
                var coordenadaCentral= new google.maps.LatLng(-0.9170800159461989, -78.63323527926615);
                var miMapa=new google.maps.Map(
                  document.getElementById('mapa'),
                  {
                    center: coordenadaCentral,
                    zoom: 14,
                    mapTypeId:google.maps.MapTypeId.ROADMAP,
                  }
                );
                var marcador = new google.maps.Marker(
                  {
                    position: coordenadaCentral,
                    map:miMapa,
                    title:'Selecciona la ubicación',
                    draggable:true
                  }
                );

                google.maps.event.addListener(
                  marcador,
                  'dragend',
                  function(event){
                    var latitud=this.getPosition().lat();
                    var longitud=this.getPosition().lng();
                    document.getElementById('latitud').value=latitud
                    document.getElementById('longitud').value=longitud
                  }
                );
              }
            </script>
            <script type="text/javascript">
$("#frm_nuevo_corresponsal").validate({
  rules:{
    "id_provincia":{
      required:true
    },
    "ciudad":{
      required:true,
    },
    "telefono":{
      required:true,
      minlength:9,
      maxlength:10
    },
    "tipoEntidad":{
      required:true
    },
    "logo":{
      required:true
    }, // Agrega una coma aquí
    "latitud":{
      required:true
    },
    "longitud":{
      required:true
    }
  },
  messages:{
    "id_provincia":{
      required:"Debe seleccionar la provincia"
    },
    "ciudad":{
      required:"Ingrese la ciudad",
    },
    "telefono":{
      required:"Ingrese el teléfono del corresponsal",
      minlength:"El teléfono debe tener un mínimo de 9 números",
      maxlength:"El teléfono debe tener un máximo de 10 números"
    },
    "tipoEntidad":{
      required:"Ingrese el tipo de entidad del corresponsal"
    },
    "logo":{
      required:"Selecciones el logo de entidad del corresponsal"
    },
    "latitud":{
      required:"Ingrese la latitud"
    },
    "longitud":{
      required:"Ingrese la longitud"
    }
  }
});
</script>
