<?php
    error_reporting(0);
  /**
   *
   */
  class Corresponsales extends CI_Controller
  {

    function __construct(){
      parent::__construct();
      $this->load->model("Corresponsal");


    }

    public function index(){
      $data["listadoJugadores"]=$this->Corresponsal->consultarTodos();
      $this->load->view('header');
      $this->load->view('corresponsales/index',$data);
      $this->load->view('footer');
    }

    //Eliminacion de hospitales con get ID
    public function borrar($id){
      $this->Corresponsal->eliminar($id);
      $this->session->set_flashdata("confirmacion","Jugador eliminado exitosamente");
      redirect("corresponsales/index");
    }

    //Renderización formulario de nuevo Hospital
    public function nuevo(){
      $data["listadoPosicion"]=$this->Corresponsal->consultarPosicion();
      $data["listadoEquipo"]=$this->Corresponsal->consultarEquipo();
      $this->load->view("header");
      $this->load->view("corresponsales/nuevo",$data);
      $this->load->view("footer");
    }

    //capturando datos e insertanfo en la tabla
    public function guardarJugador(){
      /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/
      //$config['upload_path']=APPPATH.'../uploads/corresponsales/'; //ruta de subida de archivos
      //$config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
      //$config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
      //$nombre_aleatorio="doctor_".time()*rand(100,10000);//creando un nombre aleatorio
      //$config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
      //$this->load->library('upload',$config);//cargando la libreria UPLOAD
      //if($this->upload->do_upload("logo")){ //intentando subir el archivo
      //    $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
      //    $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
      //  }else{
      //    $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
      //  }

      $datosNuevoJugador=array(
        "apellido_jug"=>$this->input->post("apellido_jug"),
        "nombre_jug"=>$this->input->post("nombre_jug"),
        "estatura_jug"=>$this->input->post("estatura_jug"),
        "salario_jug"=>$this->input->post("salario_jug"),
        "estado_jug"=>$this->input->post("estado_jug"),
        "fk_id_pos"=>$this->input->post("id_pos"),
        "fk_id_equi"=>$this->input->post("id_equi"),

      );
      $this->Corresponsal->insertar($datosNuevoJugador);
      $this->session->set_flashdata("confirmacion","Jugador registrado exitosamente");
      redirect('corresponsales/index');

    }


    //editar
    public function editar($id){
      $data["jugadorEditar"]=$this->Corresponsal->obtenerPorId($id);
      $data["listadoEquipo"]=$this->Corresponsal->consultarEquipo();
      $data["listadoPosicion"]=$this->Corresponsal->consultarPosicion();
      $this->load->view('header');
      $this->load->view('corresponsales/editar',$data);
      $this->load->view('footer');
    }

    public function actualizarJugador(){

      $id=$this->input->post("id_jug");
      $datosJugador=array(
        "apellido_jug"=>$this->input->post("apellido_jug"),
        "nombre_jug"=>$this->input->post("nombre_jug"),
        "estatura_jug"=>$this->input->post("estatura_jug"),
        "salario_jug"=>$this->input->post("salario_jug"),
        "estado_jug"=>$this->input->post("estado_jug"),
        "fk_id_pos"=>$this->input->post("id_pos"),
        "fk_id_equi"=>$this->input->post("id_equi"),

      );
      $this->Corresponsal->actualizar($id, $datosJugador);
      $this->session->set_flashdata("confirmacion","Jugador actualizado exitosamente");
      redirect('corresponsales/index');
    }




  }// fin de clase


 ?>
