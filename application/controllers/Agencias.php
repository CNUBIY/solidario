<?php
    error_reporting(0);
  /**
   *
   */
  class Agencias extends CI_Controller
  {

    function __construct(){
      parent::__construct();
      $this->load->model("Agencia");


    }

    public function index(){
      $data["listadoEquipos"]=$this->Agencia->consultarTodos();
      $this->load->view('header');
      $this->load->view('agencias/index',$data);
      $this->load->view('footer');
    }

    //Eliminacion de hospitales con get ID
    public function borrar($id){
      $this->Agencia->eliminar($id);
      $this->session->set_flashdata("confirmacion","Equipo eliminado exitosamente");
      redirect("agencias/index");
    }

    //Renderización formulario de nuevo Hospital
    public function nuevo(){
      //$data["listadoAgencias"]=$this->Agencia->consultarTodos();
      //$data["listadoProvincias"]=$this->Agencia->consultarProvincias();
      $this->load->view("header");
      $this->load->view("agencias/nuevo",$data);
      $this->load->view("footer");
    }

    //capturando datos e insertanfo en la tabla
    public function guardarEquipo(){

      $datosNuevoEquipo=array(
        "nombre_equi"=>$this->input->post("nombre_equi"),
        "siglas_equi"=>$this->input->post("siglas_equi"),
        "fundacion_equi"=>$this->input->post("fundacion_equi"),
        "region_equi"=>$this->input->post("region_equi"),
        "numero_titulos_equi"=>$this->input->post("numero_titulos_equi"),
      );
      $this->Agencia->insertar($datosNuevoEquipo);
      $this->session->set_flashdata("confirmacion","Equipo guardado exitosamente");
      redirect('agencias/index');

    }


    //editar
    public function editar($id){
      $data["equipoEditar"]=$this->Agencia->obtenerPorId($id);
      $this->load->view('header');
      $this->load->view('agencias/editar',$data);
      $this->load->view('footer');
    }

    public function actualizarEquipo(){
      $id=$this->input->post("id_equi");
      $datosEquipo=array(
        "nombre_Equi"=>$this->input->post("nombre_equi"),
        "siglas_equi"=>$this->input->post("siglas_equi"),
        "fundacion_equi"=>$this->input->post("fundacion_equi"),
        "region_equi"=>$this->input->post("region_equi"),
        "numero_titulos_equi"=>$this->input->post("numero_titulos_equi"),
      );
      $this->Agencia->actualizar($id, $datosEquipo);
      $this->session->set_flashdata("confirmacion","Equipo actualizado exitosamente");
      redirect('agencias/index');
    }




  }// fin de clase


 ?>
