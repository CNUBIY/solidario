<?php
defined('BASEPATH') OR exit('No direct script access allowed');
  error_reporting(0);
class Mapas extends CI_Controller {


    function __construct(){
      parent::__construct();
      $this->load->model("Corresponsal");
      $this->load->model("Cajero");
      $this->load->model("Agencia");


    }


   public function index(){
     $data["listadoAgencias"]=$this->Agencia->consultarTodos();
     $data["listadoCorresponsal"]=$this->Corresponsal->consultarTodos();
     $data["listadoCajeros"]=$this->Cajero->consultarTodos();
     $this->load->view('header');
     $this->load->view('mapas/index',$data);
     $this->load->view('footer');
   }
}
