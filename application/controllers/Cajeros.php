<?php
    error_reporting(0);
  /**
   *
   */
  class Cajeros extends CI_Controller
  {

    function __construct(){
      parent::__construct();
      $this->load->model("Cajero");


    }

    public function index(){
      $data["listadoPosiciones"]=$this->Cajero->consultarTodos();
      $this->load->view('header');
      $this->load->view('cajeros/index',$data);
      $this->load->view('footer');
    }

    //Eliminacion de hospitales con get ID
    public function borrar($id){
      $this->Cajero->eliminar($id);
      $this->session->set_flashdata("confirmacion","Posición eliminada exitosamente");
      redirect("cajeros/index");
    }

    //Renderización formulario de nuevo Hospital
    public function nuevo(){
      $data["listadoCajeros"]=$this->Cajero->consultarTodos();
      $this->load->view("header");
      $this->load->view("cajeros/nuevo",$data);
      $this->load->view("footer");
    }

    //capturando datos e insertanfo en la tabla
    public function guardarPosicion(){

      $datosNuevaPosicion=array(
        "nombre_pos"=>$this->input->post("nombre_pos"),
        "descripcion_pos"=>$this->input->post("descripcion_pos"),
      );
      $this->Cajero->insertar($datosNuevaPosicion);
      $this->session->set_flashdata("confirmacion","Posición guardada exitosamente");
      redirect('cajeros/index');

    }


    //editar
    public function editar($id){
      $data["posicionEditar"]=$this->Cajero->obtenerPorId($id);
      $this->load->view('header');
      $this->load->view('cajeros/editar',$data);
      $this->load->view('footer');
    }

    public function actualizarPosicion(){
      $id=$this->input->post("id_pos");
      $datosPosicion=array(
        "nombre_pos"=>$this->input->post("nombre_pos"),
        "descripcion_pos"=>$this->input->post("descripcion_pos"),
      );
      $this->Cajero->actualizar($id, $datosPosicion);
      $this->session->set_flashdata("confirmacion","Posición actualizada exitosamente");
      redirect('cajeros/index');
    }




  }// fin de clase


 ?>
