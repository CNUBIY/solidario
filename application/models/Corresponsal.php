<?php
  /**
   *
   */
  class Corresponsal extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    //nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("jugador",$datos);
      return $respuesta;
    }

    //consulta de datos
    function consultarTodos(){
      $this->db->select('jugador.*, equipo.nombre_equi as nombre_equi, posicion.nombre_pos as nombre_pos');
      $this->db->from('jugador');
      $this->db->join('equipo', 'jugador.fk_id_equi = equipo.id_equi'); // Asumiendo que equipo_id es la clave foránea en jugador
      $this->db->join('posicion', 'jugador.fk_id_pos = posicion.id_pos'); // Asumiendo que posicion_id es la clave foránea en jugador
      $query = $this->db->get();
    
      if($query->num_rows()>0){
        return $query->result();
      } else {
        return false;
      }
    }

    function consultarEquipo(){
        $query = $this->db->get("equipo");
        if($query->num_rows() > 0){
            return $query->result();
        } else {
            return false;
        }
    }
    function consultarPosicion(){
      $query = $this->db->get("posicion");
      if($query->num_rows() > 0){
          return $query->result();
      } else {
          return false;
      }
  }

    //eliminar datos
    function eliminar($id){
      $this->db->where("id_jug",$id);
      return $this->db->delete("jugador");
    }



    //consulta de datos
    function obtenerPorId($id){
      $this->db->where("id_jug",$id);
      $corresponsal=$this->db->get("jugador");
      if($corresponsal->num_rows()>0){
        return $corresponsal->row();
      } else {
        return false;
      }
    }


    function actualizar($id,$datos){
      $this->db->where("id_jug",$id);
      return $this->db->update("jugador",$datos);
    }










  }

 ?>
