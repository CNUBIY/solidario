<?php
  /**
   *
   */
  class Cajero extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    //nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("posicion",$datos);
      return $respuesta;
    }

    //consulta de datos
    function consultarTodos(){
      $this->db->select('posicion.*');
      $this->db->from('posicion');
      $query = $this->db->get();

      if($query->num_rows()>0){
        return $query->result();
      } else {
        return false;
      }
    }

    function consultarProvincias(){
        $query = $this->db->get("provincias");
        if($query->num_rows() > 0){
            return $query->result();
        } else {
            return false;
        }
    }

    //eliminar datos
    function eliminar($id){
      $this->db->where("id_pos",$id);
      return $this->db->delete("posicion");
    }



    //consulta de datos
    function obtenerPorId($id){
      $this->db->where("id_pos",$id);
      $cajeros=$this->db->get("posicion");
      if($cajeros->num_rows()>0){
        return $cajeros->row();
      } else {
        return false;
      }
    }


    function actualizar($id,$datos){
      $this->db->where("id_pos",$id);
      return $this->db->update("posicion",$datos);
    }










  }

 ?>
