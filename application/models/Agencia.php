<?php
  /**
   *
   */
  class Agencia extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    //nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("equipo",$datos);
      return $respuesta;
    }

    //consulta de datos
    function consultarTodos(){
      $this->db->select('equipo.*');
      $this->db->from('equipo');
      $query = $this->db->get();

      if($query->num_rows()>0){
        return $query->result();
      } else {
        return false;
      }
    }

    function consultarProvincias(){
        $query = $this->db->get("provincias");
        if($query->num_rows() > 0){
            return $query->result();
        } else {
            return false;
        }
    }

    //eliminar datos
    function eliminar($id){
      $this->db->where("id_equi",$id);
      return $this->db->delete("equipo");
    }



    //consulta de datos
    function obtenerPorId($id){
      $this->db->where("id_equi",$id);
      $agencias=$this->db->get("equipo");
      if($agencias->num_rows()>0){
        return $agencias->row();
      } else {
        return false;
      }
    }


    function actualizar($id,$datos){
      $this->db->where("id_equi",$id);
      return $this->db->update("equipo",$datos);
    }










  }

 ?>
